package Torres;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class FXMLController implements Initializable {

    @FXML
    private TextField txtcaja;

    @FXML
    private Label lblText;

    

    @FXML
    private void accionmostrar(ActionEvent event) {
        System.out.println("Show Solution");

        int num = Integer.parseInt(txtcaja.getText());
        System.out.println(txtcaja.getText());

        if (num > 10 || num <= 0) {

            lblText.setText("Rango no permitido");
            //JOptionPane.showMessageDialog(null, "Rango no permitido");
        } else {
            lblText.setText("");
            TorresFrame tf;
            tf = new TorresFrame(Integer.parseInt(txtcaja.getText()));
            tf.setVisible(true);
            tf.setBounds(0, 0, 510, 550);
            tf.setLocationRelativeTo(null);
            tf.setTitle("Solución");

        }
    }

    @FXML
    public void accionregresar(ActionEvent evento) {
        ((Node) (evento.getSource())).getScene().getWindow().hide();//cerrar ventana
        main();        
    }

    private void main() {
        Stage stage = new Stage();

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");

        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
