/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import javax.swing.JOptionPane;

/**
 *
 * @author Andresiac
 */
public class Desordered {

    private long[] a; // ref to array a
    private int nElems; // number of data items

    public Desordered(int max) {// constructor

        a = new long[max]; // create the array
        nElems = 0; // no items yet
    }
    
    public int size() {
        return this.nElems;
    }

    public int find(long searchKey) {// find specified value
        int j, pos = -1;
        String cad = "";
        for (j = 0; j < nElems; j++) // for each element,
        {
//            System.out.println("a[" + j + "]  = " + a[j]);
//            cad = cad + "a[" + j + "]  = " + a[j] +"\n";
            if (a[j] == searchKey) // found item?
            {
                break;
            }
        }

//        JOptionPane.showMessageDialog(null, cad);
        
        if (j == nElems) {
            return pos;
        } else {
            return pos;
        }

    }

    public void insert(long value) {
        a[nElems] = value;
        nElems++;
    }

    public boolean delete(long value) {
        int j;
        for (j = 0; j < nElems; j++) {// look for it
            if (value == a[j]) {
                break;
            }
        }
        if (j == nElems) {// can’t find it
            return false;
        } else {// found it
            for (int k = j; k < nElems; k++) {// move higher ones down
                a[k] = a[k + 1];
            }
            nElems--;// decrement size
            return true;
        }
    }

    public void display() // displays array contents
    {
        for (int j = 0; j < nElems; j++) // for each element,
        {
            System.out.print(a[j] + " "); // display it 
            JOptionPane.showMessageDialog(null, a[j] + " ");
            System.out.println("");
        }
    }
    
    public long getNum(int pos){
       return a[pos];
    }
}
