package Arrays;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class ArraysController implements Initializable {

    @FXML
    private Button btnOrd, btnDes, btnDel, btnIns, btnSearch;
    @FXML
    private TextField txtNum;
    @FXML
    private RadioButton rbLinear, rbBinary;
    @FXML
    private Label lbl1, lbl10, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lblInd, flecha;
    int x = 0, suma = 0;
    OrdArray a = new OrdArray(10);
    Desordered b = new Desordered(10);
    boolean band = true;

    @FXML
    private void orderedArray(ActionEvent event) {
        btnOrd.setDisable(true);
        btnDes.setDisable(true);
        btnDel.setDisable(false);
        btnIns.setDisable(false);
        btnSearch.setDisable(false);
        txtNum.setDisable(false);
        rbLinear.setDisable(false);
        rbBinary.setDisable(false);
        band = true;
    }

    @FXML
    private void desorderedArray(ActionEvent event) {
        btnOrd.setDisable(true);
        btnDes.setDisable(true);
        btnDel.setDisable(false);
        btnIns.setDisable(false);
        btnSearch.setDisable(false);
        txtNum.setDisable(false);
        band = false;
    }

    @FXML
    private void insert(ActionEvent event) {
        int n = Integer.parseInt(txtNum.getText());
        int pos;

        if (suma != 10) {
            if (!txtNum.getText().equals("")) {
                x = Integer.parseInt(txtNum.getText());

                if (band) {
                    pos = a.insert(x);
                    suma++;
                    lblInd.setText("Se insertó el número " + x);
                    llenarVectorA();
                } else {
                    b.insert(x);
                    suma++;
                    lblInd.setText("Se insertó el número " + x);
                    llenarVectorB();
                }
            } else {
                lblInd.setText("Escriba un número.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "El arreglo esta lleno. Solo se pueden insertar 10 valores.");
        }
    }

    @FXML
    private void delete(ActionEvent event) {
        boolean x;
        if (!txtNum.getText().equals("")) {
            if (band) {
                x = a.delete(Integer.parseInt(txtNum.getText()));
                if (x) {
                    lblInd.setText("Se borró número.");
                    llenarVectorA();
                } else {
                    lblInd.setText("No se encontró  número.");
                }
            } else {
                x = b.delete(Integer.parseInt(txtNum.getText()));
                if (x) {
                    lblInd.setText("Se borró número.");
                    llenarVectorB();
                } else {
                    lblInd.setText("No se encontró  número.");
                }
            }
        } else {
            lblInd.setText("Escriba un número.");
        }
    }

    @FXML
    private void search(ActionEvent event) {
        int pos;
        flecha.setMaxSize(lbl1.getMaxWidth(), lbl2.getMaxHeight());
        flecha.setVisible(true);
        if (band) {
            if (rbBinary.isSelected()) {
                if (!txtNum.getText().equals("")) {
                    pos = a.findBin(Integer.parseInt(txtNum.getText()));
                    lblInd.setText("Se encontró número.");
                    found(pos);
                } else {
                    lblInd.setText("Escriba un número.");
                }
            } else if (rbLinear.isSelected()) {
                if (!txtNum.getText().equals("")) {
                    int i = a.linearSearch(Integer.parseInt(txtNum.getText()));
                    if (i != -1) {
                        found(i);
                        lblInd.setText("Se encontró número.");
                    } else {
                        lblInd.setText("No se encontró número.");
                    }
                } else {
                    lblInd.setText("Escriba un número.");
                }
            } else {
                lblInd.setText("Selecciona una opción.");
            }
        } else {
            if (!txtNum.getText().equals("")) {
                int i = b.find(Long.parseLong(txtNum.getText()));
                if (i != -1) {
                    found(i);
                    lblInd.setText("Se encontró número.");
                } else {
                    lblInd.setText("No se encontró número.");
                }
            } else {
                lblInd.setText("Escriba un número.");
            }
        }

    }

    private void llenarVectorA() {
        switch (a.size()) {
            case 0:
                lbl1.setText("");
                break;
            case 1:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText("");
                break;
            case 2:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText("");
                break;
            case 3:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText("");
                break;
            case 4:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText(a.getNum(3) + "");
                lbl5.setText("");
                break;
            case 5:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText(a.getNum(3) + "");
                lbl5.setText(a.getNum(4) + "");
                lbl6.setText("");
                break;
            case 6:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText(a.getNum(3) + "");
                lbl5.setText(a.getNum(4) + "");
                lbl6.setText(a.getNum(5) + "");
                lbl7.setText("");
                break;
            case 7:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText(a.getNum(3) + "");
                lbl5.setText(a.getNum(4) + "");
                lbl6.setText(a.getNum(5) + "");
                lbl7.setText(a.getNum(6) + "");
                lbl8.setText("");
                break;
            case 8:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText(a.getNum(3) + "");
                lbl5.setText(a.getNum(4) + "");
                lbl6.setText(a.getNum(5) + "");
                lbl7.setText(a.getNum(6) + "");
                lbl8.setText(a.getNum(7) + "");
                lbl9.setText("");
                break;
            case 9:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText(a.getNum(3) + "");
                lbl5.setText(a.getNum(4) + "");
                lbl6.setText(a.getNum(5) + "");
                lbl7.setText(a.getNum(6) + "");
                lbl8.setText(a.getNum(7) + "");
                lbl9.setText(a.getNum(8) + "");
                lbl10.setText("");
                break;
            case 10:
                lbl1.setText(a.getNum(0) + "");
                lbl2.setText(a.getNum(1) + "");
                lbl3.setText(a.getNum(2) + "");
                lbl4.setText(a.getNum(3) + "");
                lbl5.setText(a.getNum(4) + "");
                lbl6.setText(a.getNum(5) + "");
                lbl7.setText(a.getNum(6) + "");
                lbl8.setText(a.getNum(7) + "");
                lbl9.setText(a.getNum(8) + "");
                lbl10.setText(a.getNum(9) + "");
                break;
            default:

        }
    }

    private void llenarVectorB() {
        switch (b.size()) {
            case 0:
                lbl1.setText("");
                break;
            case 1:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText("");
                break;
            case 2:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText("");
                break;
            case 3:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText("");
                break;
            case 4:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText(b.getNum(3) + "");
                lbl5.setText("");
                break;
            case 5:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText(b.getNum(3) + "");
                lbl5.setText(b.getNum(4) + "");
                lbl6.setText("");
                break;
            case 6:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText(b.getNum(3) + "");
                lbl5.setText(b.getNum(4) + "");
                lbl6.setText(b.getNum(5) + "");
                lbl7.setText("");
                break;
            case 7:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText(b.getNum(3) + "");
                lbl5.setText(b.getNum(4) + "");
                lbl6.setText(b.getNum(5) + "");
                lbl7.setText(b.getNum(6) + "");
                lbl8.setText("");
                break;
            case 8:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText(b.getNum(3) + "");
                lbl5.setText(b.getNum(4) + "");
                lbl6.setText(b.getNum(5) + "");
                lbl7.setText(b.getNum(6) + "");
                lbl8.setText(b.getNum(7) + "");
                lbl9.setText("");
                break;
            case 9:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText(b.getNum(3) + "");
                lbl5.setText(b.getNum(4) + "");
                lbl6.setText(b.getNum(5) + "");
                lbl7.setText(b.getNum(6) + "");
                lbl8.setText(b.getNum(7) + "");
                lbl9.setText(b.getNum(8) + "");
                lbl10.setText("");
                break;
            case 10:
                lbl1.setText(b.getNum(0) + "");
                lbl2.setText(b.getNum(1) + "");
                lbl3.setText(b.getNum(2) + "");
                lbl4.setText(b.getNum(3) + "");
                lbl5.setText(b.getNum(4) + "");
                lbl6.setText(b.getNum(5) + "");
                lbl7.setText(b.getNum(6) + "");
                lbl8.setText(b.getNum(7) + "");
                lbl9.setText(b.getNum(8) + "");
                lbl10.setText(b.getNum(9) + "");
                break;
            default:

        }
    }

    public void found(int pos) {

        switch (pos + 1) {

            case 1:
                flecha.setLayoutX(lbl1.getLayoutX() + 59);
                flecha.setLayoutY(lbl1.getLayoutY());
                break;
            case 2:
                flecha.setLayoutX(lbl2.getLayoutX() + 59);
                flecha.setLayoutY(lbl2.getLayoutY());
                break;
            case 3:
                flecha.setLayoutX(lbl3.getLayoutX() + 59);
                flecha.setLayoutY(lbl3.getLayoutY());
                break;
            case 4:
                flecha.setLayoutX(lbl4.getLayoutX() + 59);
                flecha.setLayoutY(lbl4.getLayoutY());
                break;
            case 5:
                flecha.setLayoutX(lbl5.getLayoutX() + 59);
                flecha.setLayoutY(lbl5.getLayoutY());
                break;
            case 6:
                flecha.setLayoutX(lbl6.getLayoutX() + 59);
                flecha.setLayoutY(lbl6.getLayoutY());
                break;
            case 7:
                flecha.setLayoutX(lbl7.getLayoutX() + 59);
                flecha.setLayoutY(lbl7.getLayoutY());
                break;
            case 8:
                flecha.setLayoutX(lbl8.getLayoutX() + 59);
                flecha.setLayoutY(lbl8.getLayoutY());
                break;
            case 9:
                flecha.setLayoutX(lbl9.getLayoutX() + 59);
                flecha.setLayoutY(lbl9.getLayoutY());
                break;
            case 10:
                flecha.setLayoutX(lbl10.getLayoutX() + 59);
                flecha.setLayoutY(lbl10.getLayoutY());
                break;

        }
    }
    @FXML
    public void accionRegresar(ActionEvent evento) {
        ((Node)(evento.getSource())).getScene().getWindow().hide();//cerrar ventana
        main();
        
    }
    private void main() {
        Stage stage = new Stage();        
        
        
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Expresiones.FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/StylesMenu.css");
        
        stage.setTitle("Menú Principal");
        stage.setScene(scene);
        stage.show();
        
    }
    
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
