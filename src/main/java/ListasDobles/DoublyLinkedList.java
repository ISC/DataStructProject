/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListasDobles;

/**
 *
 * @author CinthyaLiliana
 */
public class DoublyLinkedList {

    private Link first;               // ref to first item
    private Link last;                // ref to last item
// -------------------------------------------------------------

    public DoublyLinkedList() // constructor
    {
        first = null;                  // no items on list yet
        last = null;
    }
// -------------------------------------------------------------

    public boolean isEmpty() // true if no links
    {
        return first == null;
    }
// -------------------------------------------------------------

    public void insertFirst(long dd) // insert at front of list
    {
        Link newLink = new Link(dd);   // make new link

        if (isEmpty()) // if empty list,
        {
            last = newLink;             // newLink <-- last
        } else {
            first.previous = newLink;   // newLink <-- old first
        }
        newLink.next = first;          // newLink --> old first
        first = newLink;               // first --> newLink
    }
// -------------------------------------------------------------

    public void insertLast(long dd) // insert at end of list
    {
        Link newLink = new Link(dd);   // make new link
        if (isEmpty()) // if empty list,
        {
            first = newLink;            // first --> newLink
        } else {
            last.next = newLink;        // old last --> newLink
            newLink.previous = last;    // old last <-- newLink
        }
        last = newLink;                // newLink <-- last
    }
// -------------------------------------------------------------

    public Link deleteFirst() // delete first link
    {                              // (assumes non-empty list)
        Link temp = first;
        if (first.next == null) // if only one item
        {
            last = null;                // null <-- last
        } else {
            first.next.previous = null; // null <-- old next
        }
        first = first.next;            // first --> old next
        return temp;
    }
// -------------------------------------------------------------

    public Link deleteLast() // delete last link
    {                              // (assumes non-empty list)
        Link temp = last;
        if (first.next == null) // if only one item
        {
            first = null;               // first --> null
        } else {
            last.previous.next = null;  // old previous --> null
        }
        last = last.previous;          // old previous <-- last
        return temp;
    }
// -------------------------------------------------------------
    // insert dd just after key

    public boolean insertAfter(long key, long dd) {                              // (assumes non-empty list)
        Link current = first;          // start at beginning
        while (current.dData != key) // until match is found,
        {
            current = current.next;     // move to next link
            if (current == null) {
                return false;            // didn't find it
            }
        }
        Link newLink = new Link(dd);   // make new link

        if (current == last) // if last link,
        {
            newLink.next = null;        // newLink --> null
            last = newLink;             // newLink <-- last
        } else // not last link,
        {
            newLink.next = current.next; // newLink --> old next
            // newLink <-- old next
            current.next.previous = newLink;
        }
        newLink.previous = current;    // old current <-- newLink
        current.next = newLink;        // old current --> newLink
        return true;                   // found it, did insertion
    }
// -------------------------------------------------------------

    public Link deleteKey(long key) // delete item w/ given key
    {                              // (assumes non-empty list)
        Link current = first;          // start at beginning
        while (current.dData != key) // until match is found,
        {
            current = current.next;     // move to next link
            if (current == null) {
                return null;             // didn't find it
            }
        }
        if (current == first) // found it; first item?
        {
            first = current.next;       // first --> old next
        } else // not first
        // old previous --> old next
        {
            current.previous.next = current.next;
        }

        if (current == last) // last item?
        {
            last = current.previous;    // old previous <-- last
        } else // not last
        // old previous <-- old next
        {
            current.next.previous = current.previous;
        }
        return current;                // return value
    }
// -------------------------------------------------------------

    public void displayForward() {
        System.out.print("List (first-->last): ");
        Link current = first;          // start at beginning
        while (current != null) // until end of list,
        {
            current.displayLink();      // display data
            current = current.next;     // move to next link
        }
        System.out.println("");
    }
// -------------------------------------------------------------

    public void displayBackward() {
        System.out.print("List (last-->first): ");
        Link current = last;           // start at end
        while (current != null) // until start of list,
        {
            current.displayLink();      // display data
            current = current.previous; // move to previous link
        }
        System.out.println("");
    }

    public void bubbleSort() {

        Link ultimo = last;
        while (ultimo != null) {
            Link current = first;
            while (current != null && current.next != null) {
                Link sig = current.next;
                //System.out.println("entra while");
                if (current.dData > sig.dData) {
                    //System.out.println("Entra al if");
                    long d1 = current.dData;
                    long d2 = sig.dData;
                    current.dData = d2;
                    sig.dData = d1;
                }
                current = current.next;
            }
            ultimo = ultimo.previous;
        }

    }

    public void selectionSort() {
        Link current = first;   //Nodo actual
        while (current != null && current.next != null) {   //Mientras no sean nulos...
            Link sig = current.next; //Siguiente nodo
            Link minimo = current;  //Minimo, siempre empieza donde se encuentra current
            long d1, d2 = 0;
            while (minimo != null && sig != null) {     //Mientras no sean nulos...
                if (sig.dData < minimo.dData) {   //siguiente < minimo
                    if (sig.dData < current.dData) {  //siguiente < actual
                        minimo = sig;   // Se asigna el dato siguiente a minimo
                        d1 = current.dData;    //Guarda primer dato en d1
                        d2 = minimo.dData;      //Guarda dato minimo en d2
                        current.dData = d2;     //Intercambialos de lugar
                        minimo.dData = d1;
                    }
                }
                sig = sig.next; //Se actualiza nodo siguiente
            }
            current = current.next;     //Se actualiza nodo actual
        }
    }

    public int num() {

        Link current = first;
        int i = 0;
        while (current != null) {
            i++;
            current = current.next;
        }

        return i;
    }

    public Link find(int key) // find link with given key
    { // (assumes non-empty list)
        Link current = first; // start at ‘first’
        
        while (current.dData != key) // while no match,
        {
            if (current.next == null) // if end of list,
            {
                return null; // didn’t find it
            } else // not end of list,
            {
                current = current.next; // go to next link
            }
        }
        return current; // found it
    }

    public long[] arreglo() {

        Link current = first;

        long arreglo[] = new long[30];

        for (int i = 0; i < this.num(); i++) {

            if (current != null) {

                arreglo[i] = current.dData;

            }
            current = current.next;
        }

        return arreglo;
    }

    
    public int position(int key) {

        Link current = first;

        long arreglo[] = new long[30];
        int i;
        
        for ( i = 0; i < this.num(); i++) {

            if (current.dData == key) {

                break;

            }
            current = current.next;
        }

        return i;
    }

// -------------------------------------------------------------
}  // end class DoublyLinkedList
////////////////////////////////////////////////////////////////
